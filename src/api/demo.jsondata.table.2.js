
export function jsonData2 () {
  return {
          '初中': [{
              value: '初一 ',
              label: '初一 '
            },
            {
              value: '初二 ',
              label: '初二 '
            },
            {
              value: '初三 ',
              label: '初三 '
            }
          ],
          '小学': [{
              value: '一年级',
              label: '一年级'
            },
            {
              value: '二年级',
              label: '二年级'
            },
            {
              value: '三年级',
              label: '三年级'
            },
            {
              value: '四年级',
              label: '四年级'
            },
            {
              value: '五年级',
              label: '五年级'
            },
            {
              value: '六年级',
              label: '六年级'
            }
          ],
          '点校': [{
              value: '一年级',
              label: '一年级'
            },
            {
              value: '二年级',
              label: '二年级'
            },
            {
              value: '三年级',
              label: '三年级'
            },
            {
              value: '四年级',
              label: '四年级'
            },
            {
              value: '五年级',
              label: '五年级'
            },
            {
              value: '六年级',
              label: '六年级'
            }
          ],
          '九校': [{
              value: '一年级',
              label: '一年级'
            },
            {
              value: '二年级',
              label: '二年级'
            },
            {
              value: '三年级',
              label: '三年级'
            },
            {
              value: '四年级',
              label: '四年级'
            },
            {
              value: '五年级',
              label: '五年级'
            },
            {
              value: '六年级',
              label: '六年级'
            }
          ],
          '高中': [{
              value: '高一 ',
              label: '高一 '
            },
            {
              value: '高二 ',
              label: '高二 '
            },
            {
              value: '高三 ',
              label: '高三 '
            }
          ],
          '初中': [{
              value: '职高一',
              label: '职高一'
            },
            {
              value: '职高二',
              label: '职高二'
            },
            {
              value: '职高三',
              label: '职高三'
            },
            {
              value: '职高四',
              label: '职高四'
            }
          ]
        };
}
