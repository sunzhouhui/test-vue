
export function jsonData () {
  return [{
          label: '初中',
          options: [{
              value: '侨辉初中',
              label: '侨辉初中'
            },
            {
              value: '张家界二中',
              label: '张家界二中'
            },
            {
              value: '枫香岗中学',
              label: '枫香岗中学'
            },
            {
              value: '官坪中学',
              label: '官坪中学'
            },
            {
              value: '国光实验学校',
              label: '国光实验学校'
            },
            {
              value: '合作桥中学',
              label: '合作桥中学'
            },
            {
              value: '后坪中学',
              label: '后坪中学'
            },
            {
              value: '三家馆中学',
              label: '三家馆中学'
            },
            {
              value: '张家界三中',
              label: '张家界三中'
            },
            {
              value: '沙堤中学',
              label: '沙堤中学'
            },
            {
              value: '天门初中',
              label: '天门初中'
            },
            {
              value: '永定中学',
              label: '永定中学'
            },
            {
              value: '澧兰中学',
              label: '澧兰中学'
            }
          ]
        },
        {
          label: '点校',
          options: [{
              value: '高坪小学',
              label: '高坪小学'
            },
            {
              value: '老木峪小学',
              label: '老木峪小学'
            },
            {
              value: '周家河点校',
              label: '周家河点校'
            },
            {
              value: '莫家岗点校',
              label: '莫家岗点校'
            },
            {
              value: '肖家岗点校',
              label: '肖家岗点校'
            },
            {
              value: '崇山小学',
              label: '崇山小学'
            },
            {
              value: '荷花小学',
              label: '荷花小学'
            },
            {
              value: '板坪点校',
              label: '板坪点校'
            },
            {
              value: '郝坪点校',
              label: '郝坪点校'
            },
            {
              value: '高万点校',
              label: '高万点校'
            },
            {
              value: '王家湾点校',
              label: '王家湾点校'
            },
            {
              value: '岩口点校',
              label: '岩口点校'
            },
            {
              value: '张家塔点校',
              label: '张家塔点校'
            },
            {
              value: '盐井点校',
              label: '盐井点校'
            },
            {
              value: '神堂坪点校',
              label: '神堂坪点校'
            }
          ]
        }, {
          label: '高中',
          options: [{
              value: '侨辉高中',
              label: '侨辉高中'
            },
            {
              value: '天门高中',
              label: '天门高中'
            },
            {
              value: '张家界市一中',
              label: '张家界市一中'
            },
            {
              value: '北大邦补习学校',
              label: '北大邦补习学校'
            }
          ]
        }, {
          label: '九校',
          options: [{
              value: '金海学校',
              label: '金海学校'
            },
            {
              value: '阳湖坪中心校',
              label: '阳湖坪中心校'
            },
            {
              value: '黄家铺',
              label: '黄家铺'
            },
            {
              value: '谢家垭中心校',
              label: '谢家垭中心校'
            },
            {
              value: '王家坪中心校',
              label: '王家坪中心校'
            },
            {
              value: '湖田垭中心校',
              label: '湖田垭中心校'
            },
            {
              value: '天门山中心校',
              label: '天门山中心校'
            },
            {
              value: '四都坪中心校',
              label: '四都坪中心校'
            },
            {
              value: '茅岩河中心校',
              label: '茅岩河中心校'
            },
            {
              value: '新桥中心校',
              label: '新桥中心校'
            }
          ]
        },
        {
          label: '小学',
          options: [{
              value: '沅溪小学',
              label: '沅溪小学'
            },
            {
              value: '黄家河小学',
              label: '黄家河小学'
            },
            {
              value: '三家馆完小',
              label: '三家馆完小'
            },
            {
              value: '枞茂点校',
              label: '枞茂点校'
            },
            {
              value: '澧滨小学',
              label: '澧滨小学'
            },
            {
              value: '敦谊小学',
              label: '敦谊小学'
            },
            {
              value: '崇实北校',
              label: '崇实北校'
            },
            {
              value: '崇实南校',
              label: '崇实南校'
            },
            {
              value: '北门小学',
              label: '北门小学'
            },
            {
              value: '解放小学',
              label: '解放小学'
            },
            {
              value: '民族小学',
              label: '民族小学'
            },
            {
              value: '天门小学',
              label: '天门小学'
            },
            {
              value: '永定小学',
              label: '永定小学'
            },
            {
              value: '崇文小学',
              label: '崇文小学'
            },
            {
              value: '机场小学',
              label: '机场小学'
            },
            {
              value: '大庸桥小学',
              label: '大庸桥小学'
            },
            {
              value: '西溪坪小学',
              label: '西溪坪小学'
            },
            {
              value: '彭家巷小学',
              label: '彭家巷小学'
            },
            {
              value: '杨家溪小学',
              label: '杨家溪小学'
            },
            {
              value: '关门岩小学',
              label: '关门岩小学'
            },
            {
              value: '吴家嘴小学',
              label: '吴家嘴小学'
            },
            {
              value: '枫香岗完小',
              label: '枫香岗完小'
            },
            {
              value: '三坪完小',
              label: '三坪完小'
            },
            {
              value: '谢家垭完小',
              label: '谢家垭完小'
            },
            {
              value: '尹家溪中心完小',
              label: '尹家溪中心完小'
            },
            {
              value: '二家河中心完小',
              label: '二家河中心完小'
            },
            {
              value: '后坪完小',
              label: '后坪完小'
            },
            {
              value: '武溪小学',
              label: '武溪小学'
            },
            {
              value: '侯家湾小学',
              label: '侯家湾小学'
            },
            {
              value: '八家河小学',
              label: '八家河小学'
            },
            {
              value: '沙田完小',
              label: '沙田完小'
            },
            {
              value: '希望小学（含贯坪）',
              label: '希望小学（含贯坪）'
            },
            {
              value: '合作桥完小',
              label: '合作桥完小'
            },
            {
              value: '教字垭完小',
              label: '教字垭完小'
            },
            {
              value: '兴隆明德小学',
              label: '兴隆明德小学'
            },
            {
              value: '禹溪小学',
              label: '禹溪小学'
            },
            {
              value: '罗水完小',
              label: '罗水完小'
            },
            {
              value: '桥头完小',
              label: '桥头完小'
            },
            {
              value: '罗塔坪完小',
              label: '罗塔坪完小'
            },
            {
              value: '青安坪完小',
              label: '青安坪完小'
            },
            {
              value: '三岔完小',
              label: '三岔完小'
            },
            {
              value: '沅古坪完小',
              label: '沅古坪完小'
            }
          ]
        },
        {
          label: '职高',
          options: [{
              value: '侨辉职高',
              label: '侨辉职高'
            },
            {
              value: '旅游职业学校',
              label: '旅游职业学校'
            },
            {
              value: '协力技工学校',
              label: '协力技工学校'
            }
          ]
        }
      ];
}
